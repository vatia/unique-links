{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-24.05";
    flake-utils.url = "github:numtide/flake-utils";
  };
  outputs = { self, nixpkgs, flake-utils, ... }:
    flake-utils.lib.eachDefaultSystem (system:
      let pkgs = import nixpkgs { inherit system; };
          prismaOut = pkgs.prisma-engines.out;
      in {
        devShell = pkgs.mkShell {
          buildInputs = with pkgs; [
            nodejs_22
            eslint_d
            nest-cli
            prisma-engines
            openssl
            gcc
          ];
          PRISMA_QUERY_ENGINE_LIBRARY="${prismaOut}/lib/libquery_engine.node";
          PRISMA_QUERY_ENGINE_BINARY="${prismaOut}/bin/query-engine";
          PRISMA_SCHEMA_ENGINE_BINARY="${prismaOut}/bin/schema-engine";
        };
      }
    );
}
