import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Req,
  type HttpException,
} from '@nestjs/common';
import { LinksService } from './links.service';
import { CreateLinkDto } from './dto/create-link.dto';
import { CreateLinkResponse } from './responses/create-link.response';
import { ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { FastifyRequest } from 'fastify';
import { FindPayloadResponse } from './responses/find-payload.response';

@Controller('links')
export class LinksController {
  constructor(private readonly linksService: LinksService) {}

  @Post()
  @ApiTags('Create disposable link')
  @ApiOkResponse({
    description: 'Created link',
    type: CreateLinkResponse,
  })
  createLink(
    @Req() req: FastifyRequest,
    @Body() createLinkDto: CreateLinkDto,
  ): Promise<CreateLinkResponse> {
    return this.linksService.createLink(req, createLinkDto);
  }

  @Get(':uuid')
  @ApiTags('Find disposable link by payload')
  @ApiOkResponse({
    description: 'Get payload',
    type: FindPayloadResponse,
  })
  findPayload(
    @Param('uuid') uuid: string,
  ): Promise<FindPayloadResponse | HttpException> {
    return this.linksService.findPayload(uuid);
  }
}
