import { Test, type TestingModule } from '@nestjs/testing';
import { LinksService } from './links.service';
import { describe, beforeEach, it, expect } from 'vitest';
import { PrismaService } from 'src/prisma/prisma.service';

describe('LinksService', () => {
  let service: LinksService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [LinksService, PrismaService],
    }).compile();

    service = module.get<LinksService>(LinksService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
