import { ApiResponseProperty } from '@nestjs/swagger';

export class FindPayloadResponse {
  @ApiResponseProperty()
  payload: string;
}
