import { ApiResponseProperty } from '@nestjs/swagger';

export class CreateLinkResponse {
  @ApiResponseProperty()
  url: string;
}
