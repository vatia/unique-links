import {
  HttpCode,
  HttpException,
  HttpStatus,
  Injectable,
} from '@nestjs/common';
import { type CreateLinkDto } from './dto/create-link.dto';
import { type CreateLinkResponse } from './responses/create-link.response';
import { PrismaService } from 'src/prisma/prisma.service';
import { type Link } from '@prisma/client';
import { type FastifyRequest } from 'fastify';
import { type FindPayloadResponse } from './responses/find-payload.response';

@Injectable()
export class LinksService {
  constructor(private prisma: PrismaService) {}

  async createLink(
    req: FastifyRequest,
    { payload }: CreateLinkDto,
  ): Promise<CreateLinkResponse> {
    const linkData = {
      payload,
      visited: false,
    };

    const link = await this.prisma.link.create({ data: linkData });
    const url = `${req.protocol}://${req.hostname}${req.url}/${link.uuid}`;
    return { url };
  }

  async findPayload(
    uuid: string,
  ): Promise<FindPayloadResponse | HttpException> {
    const result = await this.queryPayload(uuid);
    await this.setLinkAsVisited(uuid);

    return result;
  }

  private async queryPayload(
    uuid: string,
  ): Promise<FindPayloadResponse | HttpException> {
    try {
      const linkObject: Link = await this.prisma.link.findUniqueOrThrow({
        where: {
          uuid,
          visited: false,
        },
      });
      return {
        payload: linkObject.payload,
      };
    } catch (e) {
      throw new HttpException(HttpCode(404), HttpStatus.NOT_FOUND);
    }
  }

  private async setLinkAsVisited(uuid: string): Promise<void> {
    await this.prisma.link.update({
      data: {
        visited: true,
      },
      where: {
        uuid,
      },
    });
  }
}
