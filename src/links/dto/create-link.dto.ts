import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class CreateLinkDto {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  payload: string;
}
