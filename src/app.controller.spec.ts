import { Test, type TestingModule } from '@nestjs/testing';
import { AppController } from './app.controller';
import { describe, beforeEach, it, expect } from 'vitest';

describe('AppController', () => {
  let appController: AppController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [AppController],
    }).compile();

    appController = app.get<AppController>(AppController);
  });

  describe('Health', () => {
    it('Check health', () => {
      expect(appController.getHello()).toBe('health ok!');
    });
  });
});
