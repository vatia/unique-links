import { registerAs } from '@nestjs/config';

export default registerAs('service', () => ({
  host: process.env.SERVICE_HOST || '0.0.0.0',
  port: process.env.SERVICE_PORT || 3000,
}));
