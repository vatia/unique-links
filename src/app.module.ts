import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { LinksModule } from './links/links.module';
import { PrismaService } from './prisma/prisma.service';
import serviceConfig from './config/service.config';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [serviceConfig],
    }),
    LinksModule,
  ],
  controllers: [AppController],
  providers: [PrismaService],
})
export class AppModule {}
