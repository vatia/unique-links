import { Test, type TestingModule } from '@nestjs/testing';
import { describe, it, expect, vi, beforeAll } from 'vitest';
import {
  FastifyAdapter,
  type NestFastifyApplication,
} from '@nestjs/platform-fastify';
import { v4 as uuidv4 } from 'uuid';
import {
  HttpCode,
  HttpException,
  HttpStatus,
  ValidationPipe,
} from '@nestjs/common';
import { PrismaService } from 'src/prisma/prisma.service';
import { LinksService } from 'src/links/links.service';
import { LinksModule } from 'src/links/links.module';
import { type Link } from '@prisma/client';

const mockedPrisma: Link[] = [];

vi.mock('@prisma/client', () => ({
  PrismaClient: vi.fn().mockImplementation(() => ({
    link: {
      findUniqueOrThrow: async ({ where: { uuid } }) => {
        const value = mockedPrisma.find((val) => val.uuid === uuid);
        if (value.visited === true) {
          throw new HttpException(HttpCode(404), HttpStatus.NOT_FOUND);
        }
        return value;
      },
      findFirst: async ({ where: { payload } }) => {
        const value = mockedPrisma.find((val) => val.payload === payload);
        return value;
      },
      create: async ({ data: { payload, visited } }) => {
        const uuid = uuidv4();
        const value = {
          uuid,
          payload,
          visited,
        };
        mockedPrisma.push(value);
        return value;
      },
      update: async ({ where: { uuid } }) => {
        const valueIndex = mockedPrisma.findIndex(
          (value) => value.uuid === uuid,
        );
        mockedPrisma[valueIndex].visited = true;
      },
    },
  })),
}));

describe('Links (e2e)', () => {
  let app: NestFastifyApplication;
  let prisma: PrismaService;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [LinksModule],
      providers: [LinksService, PrismaService],
    }).compile();

    app = moduleFixture.createNestApplication<NestFastifyApplication>(
      new FastifyAdapter(),
    );

    prisma = moduleFixture.get<PrismaService>(PrismaService);
    app.useGlobalPipes(new ValidationPipe());
    await app.init();
    await app.getHttpAdapter().getInstance().ready();
  });

  describe('Create links', () => {
    it('should create link', async () => {
      const payload = uuidv4();
      const response = await app.inject({
        method: 'POST',
        url: '/links',
        body: {
          payload,
        },
      });
      const { uuid } = await prisma.link.findFirst({
        where: { payload },
      });
      const responseJson = response.json();
      expect(response.statusCode).toEqual(201);
      expect(responseJson).toMatchObject({
        url: `http://localhost:80/links/${uuid}`,
      });
    });

    it('should return 400 validation error', async () => {
      const response = await app.inject({
        method: 'POST',
        url: '/links',
      });
      expect(response.statusCode).toEqual(400);

      const responseJson = response.json();
      expect(responseJson).toMatchObject({
        message: ['payload should not be empty', 'payload must be a string'],
      });
    });
  });

  describe('Find payload', () => {
    it('should return payload', async () => {
      const payload = uuidv4();
      const linkData = {
        payload,
        visited: false,
      };
      const link = await prisma.link.create({ data: linkData });

      const response = await app.inject({
        method: 'GET',
        url: `/links/${link.uuid}`,
      });

      const responseJson = response.json();
      expect(response.statusCode).toEqual(200);
      expect(responseJson).toMatchObject({ payload });
    });

    it('should return 404 when link already visited', async () => {
      const payload = uuidv4();
      const link = await app.inject({
        method: 'POST',
        url: '/links',
        body: {
          payload,
        },
      });

      const { url } = link.json();
      await app.inject({
        method: 'GET',
        url,
      });

      const response = await app.inject({
        method: 'GET',
        url,
      });
      expect(response.statusCode).toEqual(404);
    });
  });
});
