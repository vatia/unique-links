import swc from 'unplugin-swc';
import { defineConfig } from 'vitest/config';

export default defineConfig({
  test: {
    globals: true,
    include: ['{src,test}/**/*.spec.ts'],
    root: './',
    testTimeout: 10000,
  },
  plugins: [swc.vite()],
});
