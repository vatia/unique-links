import swc from 'unplugin-swc';
import { defineConfig } from 'vitest/config';

export default defineConfig({
  test: {
    globals: true,
    include: ['{src,test}/**/*.e2e-spec.ts'],
    root: './',
    testTimeout: 10000,
  },
  plugins: [
    swc.vite({
      module: { type: 'es6' },
    }),
  ],
});
