-- CreateTable
CREATE TABLE "links" (
    "uuid" UUID NOT NULL,
    "payload" TEXT NOT NULL,
    "visited" BOOLEAN NOT NULL,

    CONSTRAINT "links_pkey" PRIMARY KEY ("uuid")
);
